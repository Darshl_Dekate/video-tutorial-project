import { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

export function AdminHome(){

    const [videos, setVideos] = useState([]);

    function LoadVideos(){
        axios({
            method:'get',
            url: 'http://127.0.0.1:5000/videos'
        })
        .then(response=>{
            setVideos(response.data);
        })
    }

    useEffect(()=>{
        LoadVideos();
    },[]);

    return(
        <div>
            <h2>Admin Home</h2>
            <div className="mb-4">
                <Link to="/add-video" className="btn btn-success">Add New Video</Link>
            </div>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Preview</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        videos.map(video=>
                             <tr key={video.VideoId}>
                                <td>{video.Title}</td>
                                <td>
                                    <iframe width="100" height="100" src={video.Url}></iframe>
                                </td>
                                <td>
                                    <Link to={`/view-video/${video.VideoId}`} className="btn btn-primary me-2"><span className="bi bi-eye"></span></Link>
                                    <Link to={`/edit-video/${video.VideoId}`} className="btn btn-warning me-2"><span className="bi bi-pen-fill"></span></Link>
                                    <Link to={`/delete-video/${video.VideoId}`} className="btn btn-danger"><span className="bi bi-trash-fill"></span></Link>
                                </td>
                             </tr>
                            )
                    }
                </tbody>
            </table>
        </div>
    )
}